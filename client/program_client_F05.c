#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define PORT 8080

char loggedUser[100];

void cpStrAt(char dest[], char from[], int start) {
    int i, j = 0;
    char temp[strlen(from)];
    strcpy(temp, from);

    for (i = start; i < strlen(temp); ++i) {
        dest[j] = temp[i];
        ++j;
    }

    dest[j] = '\0';

}

void authentication_dbf05(int sck, int argc, char *argv[]) {
    int i;
    char buff[1024];
    char usrname[100], passwd[100], isRoot[2];

    for (i = 0; i < argc; ++i) {
        if (strstr(argv[i], "-u"))           
            strcpy(usrname, argv[i+1]);
        else if (strstr(argv[i], "-p"))
            strcpy(passwd, argv[i+1]);
    }

    if (getuid() == 0)
        strcpy(isRoot, "1");
    else 
        strcpy(isRoot, "0");

    // Is root
    bzero(buff, sizeof(buff));
    strcpy(buff, isRoot);
    write(sck, buff, sizeof(buff));
    bzero(buff, sizeof(buff));
    read(sck, buff, sizeof(buff));
    
    // Username
    bzero(buff, sizeof(buff));
    strcpy(buff, usrname);
    write(sck, buff, sizeof(buff));
    bzero(buff, sizeof(buff));
    read(sck, buff, sizeof(buff));

    // Password
    bzero(buff, sizeof(buff));
    strcpy(buff, passwd);
    write(sck, buff, sizeof(buff));
    bzero(buff, sizeof(buff));
    read(sck, buff, sizeof(buff));

    // send(sck, isRoot, strlen(isRoot), 0);
    // vread = read(sck, buff, 1024);
    // puts(buff);

    // send(sck, usrname, strlen(usrname), 0);
    // vread = read(sck, buff, 1024);

    // send(sck, passwd, strlen(passwd), 0);

    // Siap menerima status
    char status[100];
    
    bzero(buff, sizeof(buff));
    strcpy(buff, "Ambil status");
    write(sck, buff, sizeof(buff));
    
    bzero(buff, sizeof(buff));
    read(sck, buff, sizeof(buff));
    strcpy(status, buff);
    
    // vread = read(sck, buff, 1024);
    // strcpy(status, buff);
    
    if (strstr(status, "1")) {
        if (strcmp(isRoot, "1") == 0) {
            printf("Berhasil login (root)\n");
            strcpy(loggedUser, "root");
        } else {
            printf("Berhasil login (%s)\n", usrname);
            strcpy(loggedUser, usrname);
        }
    } else {
        printf("Data tidak ada\n");
        strcpy(loggedUser, "\0");
    }
}

void authentication__addUser_dbf05(int sck, char usrname[], char passwd[]) {
    char buff[1024];

    // Kirim perintah
    bzero(buff, sizeof(buff));
    strcpy(buff, "add user");
    write(sck, buff, sizeof(buff));
    
    bzero(buff, sizeof(buff));
    read(sck, buff, sizeof(buff));

    // Username
    bzero(buff, sizeof(buff));
    strcpy(buff, usrname);
    write(sck, buff, sizeof(buff));
    bzero(buff, sizeof(buff));
    read(sck, buff, sizeof(buff));

    // Password
    bzero(buff, sizeof(buff));
    strcpy(buff, passwd);
    write(sck, buff, sizeof(buff));
    bzero(buff, sizeof(buff));
    read(sck, buff, sizeof(buff));
}

int main(int argc, char *argv[]) {
    // Client socket
    struct sockaddr_in adrs;
    int sck = 0; 
    int vread;
    int int_pton;
    struct sockaddr_in srv_adrs;

    sck = socket(AF_INET, SOCK_STREAM, 0);
    if(sck == -1){
        printf("Creation error\n");
        return -1;
    }
    bzero(&adrs, sizeof(adrs));
    
    // memset(&srv_adrs, '0', sizeof(srv_adrs));
    srv_adrs.sin_family = AF_INET;
    srv_adrs.sin_addr.s_addr = inet_addr("127.0.0.1");
    srv_adrs.sin_port = htons(PORT);
    
    /*
    int_pton = inet_pton(AF_INET, "127.0.0.1", &srv_adrs.sin_addr);
    if(int_pton <= 0){
        printf("Invalid address or Address not supported\n");
        return -1;
    }
    */

    if(connect(sck, (struct sockaddr *)&srv_adrs, sizeof(srv_adrs)) != 0){
        printf("Connection Failed \n");
        return -1;
    }

    // Mulai prosedur

    // Login
    authentication_dbf05(sck, argc, argv);
    
    // Mulai peritah
    while(loggedUser != NULL) {
        char command[20][1024];
        int c;
        int i = 0, j = 0;

        while (1) {
            printf("DBF05> ");
            while ((c = getchar()) != '\n') {
                if (c == ' ' || c == ';') {
                    command[i][j] = '\0';
                    ++i;
                    j = 0;
                    continue;
                }

                command[i][j++] = c;
            }

            if (strstr(command[0], "CREATE") == 0 && strstr(command[1], "USER")) {
                char usrname1[100], passwd1[100];
                strcpy(usrname1, command[2]);
                strcpy(passwd1, command[5]);

                puts(command[2]);
                puts(command[5]);
                authentication__addUser_dbf05(sck, usrname1, passwd1);
            }
        

            i = 0;
            j = 0;
        }
    }
    
    
    //========================================
    /*
    char nama[100], message[100], charStr[2];
    int c;
    
    while ((c = getchar()) != '\n') {
        sprintf(charStr, "%c", c);
        strcat(nama, charStr);
    }

    send(sck, nama, strlen(nama), 0);

    vread = read(sck, message, 1024);

    printf("%s\n", message);
    */

    close(sck);
    return 0;
}