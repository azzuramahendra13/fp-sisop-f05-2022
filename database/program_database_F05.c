#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <netinet/in.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

#define PORT 8080

char loggedUser[100];
char buff[1024];

void cpStrAt(char dest[], char from[], int start) {
    int i, j = 0;
    char temp[strlen(from)];
    strcpy(temp, from);

    for (i = start; i < strlen(temp); ++i) {
        dest[j] = temp[i];
        ++j;
    }

    dest[j] = '\0';

}

void authentication_dbf05(int n_sck) {
    char dataLogin[3][100];
    char status[100];
    
    int isAvailable = 0;

    // Is root
    bzero(buff, 1024);
    read(n_sck, buff, sizeof(buff));
    strcpy(dataLogin[0], buff);
    bzero(buff, 1024);
    write(n_sck, buff, sizeof(buff));

    // Username
    bzero(buff, 1024);
    read(n_sck, buff, sizeof(buff));
    strcpy(dataLogin[1], buff);
    bzero(buff, 1024);
    write(n_sck, buff, sizeof(buff));

    // Password
    bzero(buff, 1024);
    read(n_sck, buff, sizeof(buff));
    strcpy(dataLogin[2], buff);
    bzero(buff, 1024);
    write(n_sck, buff, sizeof(buff));

    bzero(buff, 1024);
    read(n_sck, buff, sizeof(buff));
    strcpy(status, buff);
        
    if (strstr(dataLogin[0], "1")){
        // send(n_sck, "1", strlen("1"), 0);
        bzero(buff, 1024);
        strcpy(buff, "1");
        write(n_sck, buff, sizeof(buff));
        strcpy(loggedUser, "root");

    } else { 
        // send(n_sck, "0", strlen("0"), 0);
    
        FILE *fp;
        if ((fp = fopen("databases/user_data_database/user_password.txt", "r")) == NULL)
            printf("Database tidak ada\n");

        char dataUsr[100], dataPass[100];

        while(fscanf(fp, "%s", dataUsr) != EOF) {
            fscanf(fp, "%s", dataPass);
            if (strstr(dataUsr, dataLogin[1]))
                if (strstr(dataPass, dataLogin[2])){
                    isAvailable = 1;
                    break;
                }
        }

        fclose(fp);

        if (isAvailable == 1) {
            bzero(buff, 1024);
            strcpy(buff, "1");
            write(n_sck, buff, sizeof(buff));
            strcpy(loggedUser, dataLogin[1]);
        } else {
            bzero(buff, 1024);
            strcpy(buff, "0");
            write(n_sck, buff, sizeof(buff));
        }        
    }
}

void authentication_addUser_dbf05(int n_sck) {
    char usrname[100], passwd[100];

    // Username
    bzero(buff, 1024);
    read(n_sck, buff, sizeof(buff));
    strcpy(usrname, buff);
    bzero(buff, 1024);
    write(n_sck, buff, sizeof(buff));

    // Password
    bzero(buff, 1024);
    read(n_sck, buff, sizeof(buff));
    strcpy(passwd, buff);
    bzero(buff, 1024);
    write(n_sck, buff, sizeof(buff));

    FILE *fp = fopen("databases/user_data_database/user_password.txt", "a+");

    fprintf(fp, "\n%s\t\t%s", usrname, passwd);
    fclose(fp);
}

int main(int argc, char *argv[]) {
    /*
    // Proses Daemon
    pid_t pid = fork(), sid;
    pid = fork();

    if (pid < 0) exit(EXIT_FAILURE);
    if (pid > 0) exit(EXIT_SUCCESS);

    umask(0);

    sid = setsid();
    if (sid < 0) exit(EXIT_FAILURE);
    if ((chdir("/")) < 0) exit(EXIT_FAILURE);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

   
    while (1) {
        sleep(30);
    }
    // End Daemon
    */

    // Server socket
    int srv_fd, n_sck, vread;
    struct sockaddr_in adrs, clt;
    int opt = 1;
    int bnd;
    int lst;
    int addrlen = sizeof(clt);
    
    srv_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(srv_fd == -1){
        perror("Socket failed");
        exit(EXIT_FAILURE);
    }

    bzero(&adrs, sizeof(adrs));
        
    
    if(setsockopt(srv_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
        perror("Setsockopt");
        exit(EXIT_FAILURE);
    }
    

    adrs.sin_family = AF_INET;
    adrs.sin_addr.s_addr = htonl(INADDR_ANY);
    adrs.sin_port = htons(PORT);
    
    if((bind(srv_fd, (struct sockaddr *)&adrs, sizeof(adrs))) != 0){
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    if(listen(srv_fd, 5) != 0){
        perror("Listen");
        exit(EXIT_FAILURE);
    }

    n_sck = accept(srv_fd, (struct sockaddr *)&clt, &addrlen);
    if(n_sck < 0){
        perror("Accept");
        exit(EXIT_FAILURE);
    }

    // Mulai prosedur

    // Cek data login
    authentication_dbf05(n_sck);
    
    
    // Mulai perintah
    char command[200];

    while (1) {
        bzero(buff, 1024);
        read(n_sck, buff, sizeof(buff));
        strcpy(command, buff);
        
        if (strstr(loggedUser, "root") && strstr(command, "add user")) {
            bzero(buff, 1024);
            write(n_sck, buff, sizeof(buff));

            authentication_addUser_dbf05(n_sck);
        }
            
            
    }


    // ==================================================
    /*
    char message1[300], message2[100];
    strcpy(message1, "Halo ini server\n");

    vread = read(n_sck, message2, 1024);
    cpStrAt(message2, message2, 1);

    printf("Pesan dari %s\n", message2);
    sprintf(message1, "Halo %s, ini server\n", message2);

    send(n_sck, message1, strlen(message1), 0);
    */
    close(srv_fd);
    return 0;
}